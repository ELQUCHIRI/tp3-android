package com.example.hp.tp3;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.net.URL;

public class ApiGetImagesTask extends AsyncTask<ImageView, Void, Bitmap> {

    ImageView imageView;
    Team team;

    public ApiGetImagesTask(ImageView imageView, Team team) {
        this.imageView = imageView;
        this.team = team;
    }

    @Override
    protected Bitmap doInBackground(ImageView... imageViews) {

        Bitmap bitmap = null;

        try {
            URL url = new URL(this.team.getTeamBadge());
            bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
        } catch (Exception e) {
            Log.d("error in the badge",e.toString());
        }

        return bitmap;
    }

    @Override
    protected void onPostExecute(Bitmap result) {

        imageView.setImageBitmap(result);

    }
}