package com.example.hp.tp3;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class TeamActivity extends AppCompatActivity {

    private static final String TAG = TeamActivity.class.getSimpleName();
    private TextView textTeamName, textLeague, textManager, textStadium, textStadiumLocation, textTotalScore, textRanking, textLastMatch, textLastUpdate;
    private ImageView imageBadge;

    private JSONResponseHandlerTeam responseHandlerTeam;

    private int totalPoints;
    private int ranking;
    private Match lastEvent;
    private String lastUpdate;

    private Team team;

    public static String DONE = "Executed";
    public static String FAILED = "Failed";

    private FetchTeamInformations runningTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        team = (Team) getIntent().getParcelableExtra(Team.TAG);

        this.responseHandlerTeam = new JSONResponseHandlerTeam(team);

        imageBadge = (ImageView) findViewById(R.id.imageView);

        textTeamName = (TextView) findViewById(R.id.nameTeam);
        textLeague = (TextView) findViewById(R.id.leagueName);
        textStadium = (TextView) findViewById(R.id.editStadium);
        textStadiumLocation = (TextView) findViewById(R.id.editStadiumLocation);
        textTotalScore = (TextView) findViewById(R.id.editTotalScore);
        textRanking = (TextView) findViewById(R.id.editRanking);
        textLastMatch = (TextView) findViewById(R.id.editLastMatch);
        textLastUpdate = (TextView) findViewById(R.id.editLastUpdate);

        updateView();

        final Button update = (Button) findViewById(R.id.update);

        update.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (runningTask != null)
                    runningTask.cancel(true);

                runningTask = new FetchTeamInformations();
                runningTask.execute();

            }
        });

    }

    @Override
    public void onBackPressed() {

        // Save into the DataBase
        MainActivity.DB.updateTeam(team);

        // Update the Recyclerview
        MainActivity.adapter.notifyDataSetChanged();

        super.onBackPressed();
    }

    private void updateView() {

        textTeamName.setText(team.getName());
        textLeague.setText(team.getLeague());
        textStadium.setText(team.getStadium());
        textStadiumLocation.setText(team.getStadiumLocation());
        textTotalScore.setText(Integer.toString(team.getTotalPoints()));
        textRanking.setText(Integer.toString(team.getRanking()));
        textLastMatch.setText(team.getLastEvent().toString());
        textLastUpdate.setText(team.getLastUpdate());

        new ApiGetImagesTask(imageBadge, this.team).execute();
    }

    public static void displayDialog(String title, String message, Context context, int time){

        final AlertDialog alert = new AlertDialog.Builder(context).create();
        alert.setTitle(title);
        alert.setMessage(message);
        alert.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                alert.dismiss();
            }
        }, time);
    }

    public static String loadTeamContent(Team team, JSONResponseHandlerTeam responseHandlerTeam, Context context) {

        HttpsURLConnection urlConnection = null;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        try {

//            Verify connection
            if (activeNetwork != null) {


                 // get the image

                URL url = WebServiceUrl.buildSearchTeam(team.getName());

                urlConnection = (HttpsURLConnection) url.openConnection();

                if (urlConnection.getResponseCode() == 200) {

                    InputStream response = urlConnection.getInputStream();
                    responseHandlerTeam.readJsonStream(response);

                } else {
                    TeamActivity.displayDialog("Serveurs indisponibles","Veuillez réessayer plus tard !", context, 10000);
                    return TeamActivity.FAILED;
                }

            } else {
                TeamActivity.displayDialog("Connection impossible","Connectez vous à internet !", context, 10000);
                return TeamActivity.FAILED;
            }

        } catch (Exception e) {
            return TeamActivity.FAILED;
        }
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            return TeamActivity.DONE;
        }

    }

    public static String loadTeamLastEvent(Team team, JSONResponseHandlerTeam responseHandlerTeam, Context context) {

        HttpsURLConnection urlConnection = null;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        try {

//            Verify connection
            if (activeNetwork != null) {


                 //get the last event

                URL url = WebServiceUrl.buildSearchLastEvents(team.getIdTeam());
                urlConnection = (HttpsURLConnection) url.openConnection();

                if (urlConnection != null && urlConnection.getResponseCode() == 200) {

                    InputStream response = urlConnection.getInputStream();
                    responseHandlerTeam.readJsonStreamLastEvent(response);

                } else {
                    TeamActivity.displayDialog("Serveurs indisponibles","Veuillez réessayer plus tard !", context, 10000);
                    return TeamActivity.FAILED;
                }

            } else {
                TeamActivity.displayDialog("Connection impossible","Veuillez vous connectez à internet !", context, 10000);
                return TeamActivity.FAILED;
            }

        } catch (Exception e) {
            return TeamActivity.FAILED;
        }
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            return TeamActivity.DONE;
        }

    }

    public static String loadTeamRank(Team team, JSONResponseHandlerTeam responseHandlerTeam, Context context) {

        HttpsURLConnection urlConnection = null;
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        try {


            if (activeNetwork != null) {


                URL url = WebServiceUrl.buildGetRanking(team.getIdLeague());

                urlConnection = (HttpsURLConnection) url.openConnection();

                if (urlConnection.getResponseCode() == 200) {

                    InputStream response = urlConnection.getInputStream();
                    responseHandlerTeam.readJsonStreamRank(response);

                } else {
                    TeamActivity.displayDialog("Serveurs indisponibles","Veuillez réessayer plus tard !", context, 10000);
                    return TeamActivity.FAILED;
                }

            } else {
                TeamActivity.displayDialog("Connection impossible","Veuillez vous connectez à internet !", context, 10000);
                return TeamActivity.FAILED;
            }

        } catch (Exception e) {
            return TeamActivity.FAILED;
        }
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            return TeamActivity.DONE;
        }

    }

    private final class FetchTeamInformations extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {

            // Fetch the team content such as image ...
            String string1 = loadTeamContent(team, responseHandlerTeam, TeamActivity.this);

            // Fetch last event
            String string2 = loadTeamLastEvent(team, responseHandlerTeam, TeamActivity.this);

            // Update rank
            String string3 = loadTeamRank(team, responseHandlerTeam, TeamActivity.this);

            if (string1.equals(TeamActivity.DONE) && string2.equals(TeamActivity.DONE) && string3.equals(TeamActivity.DONE)) {
                return TeamActivity.DONE;
            } else {
                return TeamActivity.FAILED;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            updateView();
        }
    }
}
